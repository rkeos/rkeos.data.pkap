# This Makefile was inspired by the Makefile of the package git2r
# (Version: 0.18.0.9000)

# Determine package name and version from DESCRIPTION file
PKG_VERSION=$(shell grep -i ^version DESCRIPTION | cut -d : -d \  -f 2)
PKG_NAME=$(shell grep -i ^package DESCRIPTION | cut -d : -d \  -f 2)

# Name of built package
PKG_TAR=$(PKG_NAME)_$(PKG_VERSION).tar.gz

#data-raw
DR=./data-raw

# Install package
install: data
	cd .. && R CMD INSTALL $(PKG_NAME)

# Download data
PKAP_MSUD=$(DR)/pkap_main_survey_unit_descriptions.csv
PKAP_MFD=$(DR)/pkap_main_finds_descriptions.csv
PKAP_BG=$(DR)/pkap_bg.tif

data: $(DR)/data-make.R
	[ -e $(PKAP_MSUD) ] || wget 'http://artiraq.org/static/opencontext/revised-tables/09c85ef2dd6784a0569fdc283a7d550c.csv' -O $(PKAP_MSUD)
	[ -e $(PKAP_MFD) ] || wget 'http://artiraq.org/static/opencontext/revised-tables/745df886025114d1fa7afdb464ac5ac6.csv' -O $(PKAP_MFD) 
	[ -e $(PKAP_BG) ] || wget 'https://gitlab.com/nehemie/copernicus-data/raw/542b1bb7cc1a4512d71ef53576937a836e3dd467/data/pyla-koutsopetria_archaeological_project/pkap_bg.tif' -O $(PKAP_BG)
	Rscript --vanilla  $(DR)/data-make.R

# Build documentation with roxygen
# 1) Remove old doc
# 2) Generate documentation
roxygen:
	rm -f man/*.Rd
	cd .. && Rscript -e "library(roxygen2); roxygenize('$(PKG_NAME)')"

# Generate PDF output from the Rd sources
# 1) Rebuild documentation with roxygen
# 2) Generate pdf, overwrites output file if it exists
pdf: roxygen
	cd .. && R CMD Rd2pdf --force $(PKG_NAME)

# Build and check package
check:
	cd .. && R CMD build $(PKG_NAME)
	cd .. && _R_CHECK_CRAN_INCOMING_=FALSE NOT_CRAN=true \
        R CMD check $(PKG_TAR)

check-no-vignette:
	cd .. && R CMD build $(PKG_NAME)
	cd .. && R CMD check --no-vignettes --no-build-vignettes $(PKG_TAR)

check-as-cran:
	cd .. && R CMD build $(PKG_NAME)
	cd .. && R CMD check --as-cran $(PKG_TAR)
