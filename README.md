# The rkeos.data.pkap package

Status: [![pipeline
status](https://gitlab.com/rkeos/rkeos.data.pkap/badges/master/pipeline.svg)](https://gitlab.com/rkeos/rkeos.data.pkap/commits/master)

This package contains some of datasets of the Pyla-Koutsopetria Archaeological Project.

>
> William R. Caraher, R. Scott Moore, David K. Pettegrew.
> "Pyla-Koutsopetria Archaeological Project". (2013) William R.
> Caraher, R. Scott Moore, David K. Pettegrew (Eds.) . Released:
> 2013-10-25. Open Context.
> <http://opencontext.org/projects/3F6DCD13-A476-488E-ED10-47D25513FCB2>
> DOI: https://doi.org/10.6078/M7B56GNS ARK (Archive):
> https://n2t.net/ark://28722/k2dj59f9n
>
>  The dataset collected by the Pyla-Koutsopetria Archaeological Project
>  (PKAP) documents fieldwork that began in the summer of 2004 near the
>  modern village of Pyla on the southern coast of Cyprus. Over seven
>  field seasons, PKAP teams documented the coastal zone of Pyla using a
>  combination of intensive pedestrian survey, geophysical prospecting,
>  and excavation. We systematically sampled 100 ha in the area,
>  recorded hundreds of thousands of artifacts on the surface, and
>  described hundreds of cut blocks and in situ architectural features. 
>
>

The source of the package (https://gitlab.com/rkeos/rkeos.data.pkap)
contains a folder `data-raw` and a script `data-make.R` explaining
which data-sets have been used and how it was packaged.

